package com.javagda25.nbpapi;

public enum DataFormat {
    XML,
    JSON;
}
