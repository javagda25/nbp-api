package com.javagda25.nbpapi;

public enum CurrencyCode {
//    PLN,
    USD,
    GBP,
    EUR,
    CHF,
    CNY,
    ZWL
}
