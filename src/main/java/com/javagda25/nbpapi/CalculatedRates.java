package com.javagda25.nbpapi;

import lombok.Data;

@Data
public class CalculatedRates {
    private Double ask, mid, bid;
}
